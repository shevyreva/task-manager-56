package ru.t1.shevyreva.tm.exception.user;

public class TimeoutException extends AbstractUserException {

    public TimeoutException() {
        super("Session is closed. Try again.");
    }

}
