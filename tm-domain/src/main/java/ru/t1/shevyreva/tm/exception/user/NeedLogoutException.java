package ru.t1.shevyreva.tm.exception.user;

public class NeedLogoutException extends AbstractUserException {

    public NeedLogoutException() {
        super("Error! You are already login. Please,logout and try again.");
    }
}
