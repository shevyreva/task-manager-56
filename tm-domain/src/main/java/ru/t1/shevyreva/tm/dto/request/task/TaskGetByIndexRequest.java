package ru.t1.shevyreva.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskGetByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public TaskGetByIndexRequest(@Nullable String token, @Nullable final Integer index) {
        super(token);
        this.index = index;
    }

}
