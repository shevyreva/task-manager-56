package ru.t1.shevyreva.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;


@Getter
@Setter
@NoArgsConstructor
public class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private UserDTO user;

    public AbstractUserResponse(@NotNull final UserDTO user) {
        this.user = user;
    }

}
