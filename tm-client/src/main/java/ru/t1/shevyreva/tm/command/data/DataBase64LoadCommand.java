package ru.t1.shevyreva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.data.DataLoadBase64Request;
import ru.t1.shevyreva.tm.enumerated.Role;

@Component
public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";
    @NotNull
    private final String DESCRIPTION = "Load Data to base64 file.";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable void execute() {
        System.out.println("[DATA BASE64 LOAD]");
        @NotNull final DataLoadBase64Request request = new DataLoadBase64Request(getToken());
        domainEndpoint.loadDataBase64(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
