package ru.t1.shevyreva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.data.DataLoadJsonFasterXmlRequest;
import ru.t1.shevyreva.tm.enumerated.Role;

@Component
public class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load Data to json file.";

    @NotNull
    private final String NAME = "data-load-json";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable void execute() {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataLoadJsonFasterXmlRequest request = new DataLoadJsonFasterXmlRequest(getToken());
        domainEndpoint.loadJsonDataFasterXml(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
