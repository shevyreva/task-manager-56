package ru.t1.shevyreva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.api.endpoint.*;
import ru.t1.shevyreva.tm.api.repository.ICommandRepository;
import ru.t1.shevyreva.tm.api.service.*;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.shevyreva.tm.exception.system.CommandNotSupportedException;
import ru.t1.shevyreva.tm.util.SystemUtil;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@NoArgsConstructor
public class Bootstrap implements IServiceLocator {

    @NotNull
    private final String PACKAGE_COMMANDS = "ru.t1.shevyreva.tm.command";

    @NotNull
    @Getter
    private ICommandRepository commandRepository;

    @NotNull
    @Getter
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Getter
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Getter
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Getter
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Getter
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Getter
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Getter
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    @SneakyThrows
    public void registry(@NotNull final AbstractCommand[] commands) {
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) return;
            commandService.add(command);
        }
    }

    public void start(@Nullable final String[] args) {
        if (processArguments(args)) System.exit(0);

        prepareStartup();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println(e.getMessage());
                System.out.println("[FAILED]");
            }
        }
    }

    public boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        processArgument(args[0]);
        return true;
    }

    public void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void prepareStartup() {

        loggerService.info("**Welcome to Task Manager**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initPID();
        registry(abstractCommands);
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("**Shutdown Task Manager**");
        fileScanner.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task_manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull File file = new File(fileName);
        file.deleteOnExit();
    }

}
