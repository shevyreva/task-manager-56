package ru.t1.shevyreva.tm.api.service;

import org.springframework.stereotype.Service;
import ru.t1.shevyreva.tm.api.repository.ICommandRepository;

@Service
public interface ICommandService extends ICommandRepository {
}
