package ru.t1.shevyreva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.List;

public interface IAbstractModelDtoRepository<M extends AbstractModelDTO> {

    void add(@NotNull final M model);

    void update(@NotNull final M model);

    void clear();

    List<M> findAll();

    M findOneById(@NotNull final String id);

    void removeOneById(@NotNull final String id);

    long getSize();

    EntityManager getEntityManager();

}
