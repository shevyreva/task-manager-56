package ru.t1.shevyreva.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.shevyreva.tm.api.repository.model.IAbstractUserOwnedModelRepository;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;
import ru.t1.shevyreva.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractUserOwnedModelRepository<M extends AbstractUserOwnedModel> extends AbstractModelRepository<M> implements IAbstractUserOwnedModelRepository<M> {

    public void addForUser(@NotNull String userId, @NotNull M model) {
        model.setUser(entityManager.find(User.class, userId));
        entityManager.persist(model);
    }

    @Override
    public void updateForUser(@NotNull String userId, @NotNull M model) {
        entityManager.persist(model);
    }

    @Override
    public abstract void clearForUser(@NotNull String userId);

    @Override
    public abstract void removeOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    public abstract List<M> findAllForUser(@NotNull String userId);

    @Override
    public abstract M findOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    public abstract int getSizeForUser(@NotNull String userId);

}
