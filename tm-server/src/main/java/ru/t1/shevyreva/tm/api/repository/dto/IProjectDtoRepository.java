package ru.t1.shevyreva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;

import java.util.Comparator;
import java.util.List;

public interface IProjectDtoRepository extends IAbstractUserOwnedModelDtoRepository<ProjectDTO> {

    @Nullable
    ProjectDTO findOneByIndexForUser(@NotNull String userId, @NotNull Integer index);

    @Nullable
    ProjectDTO findOneByIndex(@NotNull Integer index);

    boolean existsByIdForUser(@NotNull String userId, @NotNull String id);

    boolean existById(@NotNull final String id);

    List<ProjectDTO> findAll(@Nullable final Comparator<ProjectDTO> comparator);

    List<ProjectDTO> findAllForUser(@NotNull final String userId, @Nullable final Comparator<ProjectDTO> comparator);

    void removeOneByIdForUser(@NotNull String userId, @NotNull String id);

    void removeOneById(@NotNull String id);

}
