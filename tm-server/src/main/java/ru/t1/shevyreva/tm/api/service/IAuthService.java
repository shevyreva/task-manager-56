package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;
import ru.t1.shevyreva.tm.dto.model.UserDTO;

import javax.security.sasl.AuthenticationException;

public interface IAuthService {

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO check(@Nullable String password, @Nullable String login) throws AuthenticationException;

    @NotNull
    String login(@Nullable final String login, @Nullable final String password);

    @NotNull
    SessionDTO validateToken(@Nullable final String token);

    void invalidate(@Nullable final SessionDTO session);

}
