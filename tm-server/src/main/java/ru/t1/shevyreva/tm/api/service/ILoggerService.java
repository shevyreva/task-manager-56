package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void debug(@Nullable String message);

    void error(@Nullable Exception e);

    void info(@Nullable String message);

    void command(@Nullable String message);

    void initJmsLogger();

}
