package ru.t1.shevyreva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.shevyreva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.shevyreva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.shevyreva.tm.exception.field.TaskIdEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.repository.dto.ProjectDtoRepository;
import ru.t1.shevyreva.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@AllArgsConstructor
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    public IProjectDtoRepository getProjectRepository(){
        return context.getBean(IProjectDtoRepository.class);
    }

    @NotNull
    public ITaskDtoRepository getTaskRepository(){
        return context.getBean(ITaskDtoRepository.class);
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            if (!projectRepository.existsByIdForUser(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final TaskDTO task = taskRepository.findOneById(taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            if (!projectRepository.existsByIdForUser(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final TaskDTO task = taskRepository.findOneById(taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            if (!projectRepository.existsByIdForUser(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, projectId);
            for (@NotNull final TaskDTO task : tasks) {
                taskRepository.removeOneById(task.getId());
            }
            projectRepository.removeOneById(projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
