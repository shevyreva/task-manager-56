package ru.t1.shevyreva.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.shevyreva.tm.api.repository.dto.IAbstractUserOwnedModelDtoRepository;
import ru.t1.shevyreva.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;
import java.util.List;
@Repository
@Scope("prototype")
@AllArgsConstructor
public abstract class AbstractUserOwnedModelDtoRepository<M extends AbstractUserOwnedModelDTO> extends AbstractModelDtoRepository<M>
        implements IAbstractUserOwnedModelDtoRepository<M> {

    public void addForUser(@NotNull String userId, @NotNull M model) {
        model.setUserId(userId);
        entityManager.persist(model);
    }

    @Override
    public void updateForUser(@NotNull String userId, @NotNull M model) {
        entityManager.persist(model);
    }

    @Override
    public abstract void clearForUser(@NotNull String userId);

    @Override
    public abstract void removeOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    public abstract List<M> findAllForUser(@NotNull String userId);

    @Override
    public abstract M findOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    public abstract long getSizeForUser(@NotNull String userId);

}
