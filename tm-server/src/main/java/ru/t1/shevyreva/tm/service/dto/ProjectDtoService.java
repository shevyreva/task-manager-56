package ru.t1.shevyreva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.shevyreva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.dto.IProjectDtoService;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;
import ru.t1.shevyreva.tm.enumerated.ProjectDtoSort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.*;
import ru.t1.shevyreva.tm.repository.dto.ProjectDtoRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public final class ProjectDtoService implements IProjectDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    public IProjectDtoRepository getRepository(){
        return context.getBean(IProjectDtoRepository.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();


        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.addForUser(userId, project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.addForUser(userId, project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name, final String description
    ) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize(userId) < index) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize(userId) < index) throw new IndexEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final ProjectDTO project = findOneByIndex(userId, index);
        project.setStatus(status);
        if (project == null) throw new ProjectNotFoundException();

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final ProjectDTO project = findOneById(userId, id);
        project.setStatus(status);
        if (project == null) throw new ProjectNotFoundException();

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final ProjectDtoSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            if (sort == null) return projectRepository.findAllForUser(userId);
            if (sort.getDisplayName() != null) return projectRepository.findAllForUser(userId, sort.getComparator());
            else return null;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            @Nullable final ProjectDTO project = projectRepository.findOneByIdForUser(userId, id);
            if (project == null) throw new TaskNotFoundException();
            return project;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize(userId) < index) throw new IndexEmptyException();

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            @Nullable final ProjectDTO project = projectRepository.findOneByIndexForUser(userId, index);
            if (project == null) throw new TaskNotFoundException();
            return project;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            return projectRepository.getSizeForUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO removeOne(@Nullable final String userId, @Nullable final ProjectDTO project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ModelNotFoundException();

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.removeOneByIdForUser(userId, project.getId());
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            ProjectDTO project = projectRepository.findOneByIdForUser(userId, id);
            entityManager.getTransaction().begin();
            projectRepository.removeOneByIdForUser(userId, id);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.clearForUser(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public List<ProjectDTO> findAll() {
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            return projectRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Collection<ProjectDTO> add(@NotNull final Collection<ProjectDTO> models) {
        if (models == null) throw new ProjectNotFoundException();

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (@NotNull ProjectDTO project : models) {
                projectRepository.add(project);
            }
            entityManager.getTransaction().commit();
            return models;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public void removeAll() {
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

    }

    @NotNull
    @SneakyThrows
    public Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> models) {
        @Nullable final Collection<ProjectDTO> entities;
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            removeAll();
            entityManager.getTransaction().begin();
            entities = add(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return entities;
    }

    public boolean existsById(@NotNull final String user_id, @NotNull final String id) {
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            return (projectRepository.findOneByIdForUser(user_id, id) != null);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO add(@NotNull final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();

        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.add(model);

            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
